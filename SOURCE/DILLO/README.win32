=============
 Dillo-Win32
=============

  Dillo-Win32 is a fast and extremely lightweight graphical web
browser for Windows, OS X, and Unix systems.  It is based on Dillo,
but offers greater portability, a saner codebase, an improved user
interface, and more features compared to the mainline browser.

  Despite the name, most of Dillo-Win32's features are NOT platform-
specific; the name "Dillo-Win32" simply reflects the project's origins
as a Windows port of the browser.  Besides Windows, Dillo-Win32 is
regularly developed and tested on OpenBSD, and should compile and run
on any platform supported by mainline Dillo.

  For more information, pre-compiled releases, and additional
developer resources, please see:
  http://dillo-win32.sourceforge.net/

--------
Features
--------

  Dillo-Win32's feature list is constantly changing, and this list
is not exhaustive.  The most important ones are:

  - Native support for Microsoft Windows 95 / NT 4.0 and newer.

  - Required browser functionality built into the browser, not as a
    DPI (Dillo plug-in).  DPI is an extension system, and required
    functionality is by definition not an "extension."

  - Sane, user-friendly default settings.  Unlike mainline, Dillo-Win32
    does not require any extra configuration for "normal" behavior.

  - Simple built-in downloader.

  - Built-in HTTPS support, including support for CyaSSL (a lightweight
    embedded SSL library that's considerably smaller than OpenSSL).

  - Improved cookies support.  Dillo-Win32 does not write cookies to
    disk by default (this can be changed at compile time with the
    --enable-persistent-cookies configure option), which is similar
    to how other browsers behave in private browsing mode.

  - Improved bookmarks interface.  Dillo-Win32 uses traditional dialogs
    and drop-down menus rather than mainline's clunky web interface.

  - Preferences dialog.  It's 2011, and no one should have to manually
    edit configuration files -- especially not for a graphical program!

  - Limited printing support.  (This is still a work in progress, and
    does not work perfectly yet due to unidentified upstream bugs.)

  - Various other improvements, tweaks, and bugfixes over upstream.

  Developers interested in Dillo-Win32's internals should check out our
technical overview for more information:
  http://dillo-win32.sourceforge.net/dillo/tech-overview.php

------------
Dependencies
------------

  Dillo-Win32 requires patched versions of several libraries to
fix bugs in the upstream code.  Source code and pre-built binaries
are available from the following address:
  http://dillo-win32.sourceforge.net/dillo/download.php

  The built-in downloader requires libcurl (http://curl.haxx.se).
If libcurl is not available, downloads will be unavailable on Windows,
while Unix will fall back to the downloads DPI.

  HTTPS support requires either OpenSSL (http://www.openssl.org) or
CyaSSL (http://www.yassl.com).  Dillo-Win32's official binary releases
currently use CyaSSL due to its much smaller footprint.

--------
Building
--------

  To build on Windows, you must use MinGW/MSYS from mingw.org;
Visual C++ is not currently supported.  Cygwin may also work, but
it's untested since we're targeting native Win32, not emulation.

  We provide a mirror of the old but known good MinGW 5.1.4 and
MSYS 1.0.11 releases at the following address:
  http://dillo-win32.sourceforge.net/mingw/

  Dillo-Win32 does NOT include a pre-generated ./configure script.
Generate one using the autogen.sh script (requires autoconf and automake).

  Dillo-Win32 adds several new configuration options to enable/disable
various enhancements, including:

    --enable-mozilla-user-agent [enabled by default]
        Use a Mozilla/4.0 user agent string for better compatibility
        with certain sites (Google, etc.)

    --enable-internal-dlgui [enabled by default]
        Enable built-in downloads support using libcurl.
        Note: If this is disabled, Windows won't have downloads support
        at all, while Unix will fall back on the downloads DPI.

    --enable-persistent-cookies [disabled by default]
        Enable writing cookies to disk like the upstream browser does.
        Note: This has severe privacy implications, and allows only one
        Dillo instance to support cookies at a time.

    --enable-cyassl [disabled by default]
        Enable HTTPS support using CyaSSL instead of OpenSSL.

-----------
DPI Support
-----------

  Dillo-Win32 disables Dillo plug-ins (DPIs) by default.  Most DPI
functionality has been built into the browser, usually with significant
enhancements over mainline.

  DPIs can be re-enabled on Unix, but are not supported at all on Windows
due to technical limitations.  Again, note that Dillo-Win32 already builds
most DPI functionality into the browser; you do not need DPIs for basic
features like HTTPS, downloads, local file browsing, and bookmarks.

  For more information on why Dillo-Win32 does not use DPI (and our
objections to its use upstream), please read:
  http://dillo-win32.sourceforge.net/dillo/dpi.php

Benjamin Johnson
(obeythepenguin@users.sourceforge.net)
November, 2011
